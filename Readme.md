# Backend Virust Total 

## Docker
To get started quickly,in the base directory of the repository. 

__Local Devel__

```
docker-compose -f docker-compose.yml -f docker-compose-devel.yml up --build
``` 

__Deploy__

```
docker-compose -f docker-compose.yml -f docker-compose-deploy.yml up --build
```

__Unit Test__
```
docker-compose -f docker-compose-test.yml up --remove-orphans
```

