from app_main import app
import os
import unittest


class BasicTests(unittest.TestCase):

    # ###########################
    # ### setup and teardown ####
    # ###########################

    # executed prior to each test
    def setUp(self):
        os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
        app.config['SECRET_KEY'] = 'testing'
        app.config['TESTING'] = True
        self.app = app.test_client()

    # executed after each test
    def tearDown(self):
        pass

    # ##############
    # ### tests ####
    # ##############

    def test_authorize_url_redirect(self):
        response = self.app.get('/authorize')
        self.assertTrue(response.headers.get("Location").startswith("https://accounts.google.com/o/oauth2/auth"))
        self.assertEqual(response.status_code, 302)

    def test_oauth2_url_redirect(self):
        """This test case doesn't work but it depends on google more than us"""
        with self.app as c:
            with c.session_transaction() as sess:
                sess['state'] = "test"
            # response = c.get('/oauth2callback')
            self.assertEqual(404, 302)


if __name__ == "__main__":
    unittest.main()
