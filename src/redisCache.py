# -*- coding: utf-8 -*-
"""Cache for application implemented in Redis

"""
import json
from redis import Redis

CACHE_TIMEOUT = 10


class RedisInterface():
    serializer = json

    def __init__(self, redis=None, prefix='cache:'):
        """Initialize the object with the type of data

        @author Joel Braun

        Named Params:
        redis: Connection to the redis server
        prefix: name to add to Redis key name
        """
        if redis is None:
            redis = Redis()
        self.redis = redis
        self.prefix = prefix

    def get_cache(self, cid):
        """Retrieve data fromt the cache for the user

        @author Joel Braun

        Named Params:
        cid: user ID of data owner
        """
        if cid is not None:
            vt_data = self.redis.get(self.prefix + cid)
            if vt_data is not None:
                data = self.serializer.loads(vt_data.decode())
            else:
                data = []
            return data
        return []

    def save_cache(self, cid, data):
        """Save data into redis with the CID

        @author Joel Braun

        Named Params:
        cid: user ID of data owner
        data: What to save in the redis
        """
        self.redis.delete(self.prefix + cid)
        val = self.serializer.dumps(data)
        self.redis.setex(self.prefix + cid, val, CACHE_TIMEOUT)


redis_conn = Redis(host='redis', port=6379)
virustotal_cache = RedisInterface(redis=redis_conn, prefix="vtCache:")
gdrive_cache = RedisInterface(redis=redis_conn, prefix="gdCache:")
