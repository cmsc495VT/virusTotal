import virustotal
import unittest

folder = {'mimeType': 'application/vnd.google-apps.folder'}
pdf_file_unscanned = {'mimeType': 'application/pdf',
                      'fileId': '12345',
                      'scan': {
                        'response_code': 0
                      }}
pdf_file_scanned = {'mimeType': 'application/pdf',
                    'fileId': '12346',
                    'scan': {
                        'response_code': 1,
                        'scan_date': '2017-11-13 16:27:41',
                        'total': 4,
                        'positives': 2,
                        'permalink': 'http://test.com',
                        'scans': 4
                    }}
pdf_file_corrupt_scanned = {'mimeType': 'application/pdf',
                            'fileId': '12346',
                            'scan': {
                                'response_code': 1,
                                'scan_date': '2017-11-13 16:27:41',
                                'total': 4,
                                'positives': 2,
                                'permalink': 'http://test.com',
                                'ERROR_KEY_MISSING_SCANS': 4
                            }}

mixed_vt_results = [folder, pdf_file_unscanned]


class BasicTests(unittest.TestCase):

    # ###########################
    # ### setup and teardown ####
    # ###########################

    # executed prior to each test
    def setUp(self):
        pass

    # executed after each test
    def tearDown(self):
        pass

    # ##############
    # ### tests ####
    # ##############
    """ TODO: Database needs to be mocked up for testing scan_file()
        TODO: vt_check() should just make the list and not send to vt_get_scan_report
                   to make it testable
    """

    def test_folder_trimming_results_to_frontend(self):
        # Trim a file if the only result
        returns = virustotal.vt_results_to_frontend([folder])
        self.assertEqual(len(returns), 0)
        # Trim from a list of results
        returns = virustotal.vt_results_to_frontend(mixed_vt_results)
        self.assertEqual(len(returns), len(mixed_vt_results) - 1)

    def test_result_cleaning_vt_to_frontend(self):
        # Test an unscanned file
        returns = virustotal.vt_to_frontend(pdf_file_unscanned)
        self.assertEqual(returns['scanned'], 0)
        # Test a scanned file
        returns = virustotal.vt_to_frontend(pdf_file_scanned)
        self.assertEqual(returns['totalScans'], 4)
        # Test a scanned file with a corrupt result
        returns = virustotal.vt_to_frontend(pdf_file_corrupt_scanned)
        self.assertEqual(returns['scanned'], 0)


if __name__ == "__main__":
    unittest.main()
