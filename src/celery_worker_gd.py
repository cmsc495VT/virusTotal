# -*- coding: utf-8 -*-
"""Celery async workerers

Celery tasks that take a long time to execute that we push to to async workers
"""
from celery import Celery
import database
import google_drive
from os import getenv

app = Celery('celery_worker_gd')
app.conf.broker_url = getenv('REDIS_BROKER_URL')
app.conf.result_backend = getenv('REDIS_RESULT_BACKEND_URL')


@app.task
def google_drive_updates(client_credentials):
    file_list = google_drive.gdrive_file_list(client_credentials)
    user_id = client_credentials['user_id']
    database.save_files(user_id, file_list)
