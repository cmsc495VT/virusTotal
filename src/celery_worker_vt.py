# -*- coding: utf-8 -*-
"""Celery async workerers

Celery tasks that take a long time to execute that we push to to async workers
"""
from celery import Celery
import google.oauth2.credentials
import database
import google_drive
import redisCache
import virustotal
import time
from os import getenv

app = Celery('celery_worker_vt')
app.conf.broker_url = getenv('REDIS_BROKER_URL')
app.conf.result_backend = getenv('REDIS_RESULT_BACKEND_URL')


@app.task
def virus_total_content_scan(file, client_credentials):
    """Scan the contents of a file in the google drive

    @author Joel Braun, Micah Strube

    :file: Dict of the file object
    :client_credentials: Dict of client credentials for google accounts
    :Returns: Nothing
    """
    print("Checking VT for {0}".format(file['fileName']))
    google_credentials = google.oauth2.credentials.Credentials(client_credentials['token'],
                                                               client_credentials['refresh_token'],
                                                               client_credentials['id_token'],
                                                               client_credentials['token_uri'],
                                                               client_credentials['client_id'],
                                                               client_credentials['client_secret'],
                                                               client_credentials['scopes'])

    file_content = google_drive.gdrive_file_content(google_credentials, file['fileId'], file['mimeType'])
    submitted_md5, permalink = virustotal.content_scan(file['fileId'], file['fileName'], file_content)
    vt_scan_response = virustotal.vt_check(submitted_md5)

    # If the file is a Google App, we don't have the md5 yet so grab it from
    # the VT response and update the database
    if 'application/vnd.google-apps' in file['mimeType']:
        print("Updating databased md5 to {md5} for Google App file {fileId}"\
                .format(md5=submitted_md5, fileId=file['fileId']))
        database.update_file_md5(file['fileId'], submitted_md5)
        file['md5Checksum'] = submitted_md5

    # Seconds to wait between polls for scan results
    sleep_time = 5
    # Wait until scan is complete
    while vt_scan_response['response_code'] != 1:
        print("VT did not finish scanning file {0}. Checking again in {1} seconds. VT Permalink: {2}".format(file['fileId'], sleep_time, permalink))
        # Set files scanned status to -1 (in progress)
        database.update_file_scanned_status(file['fileId'], "-1")
        # Wait, then check again
        time.sleep(sleep_time)
        vt_scan_response = virustotal.vt_check(submitted_md5)
    print("VT finished scanning file {0}".format(file['fileId']))
    virustotal.scan_file(file)


@app.task
def virus_total_cache(user_id, cache_id):
    """Update the cache from the Virus mongo database

    @author Joel Braun

    :user_id: Client id of the user, needed query the database
    :cache_id: ID of the user's cache
    :Returns: Nothing
    """
    vt_results = database.vt_get_scans(user_id)
    if len(vt_results) > 0:
        redisCache.virustotal_cache.save_cache(cache_id, vt_results)


@app.task
def virus_total_scan(file):
    """Check Virustotal to see if the hash has a report

    @author Joel Braun

    :file: Dict of a file object
    :Returns: Nothing
    """
    print("Checking VT for file {filename}, md5 {md5}".format(md5=file['md5Checksum'], filename=file['fileName']))
    virustotal.scan_file(file)


@app.task
def virus_total_queue(user_id):
    """Check Virustotal to see if any of the client's files' hashes have a report

    @author Joel Braun

    :user_id: Id of the user to query the mongo database with
    :Returns: Nothing
    """
    files = database.get_files(user_id)
    for file in files:
        if 'scan' not in file and file['mimeType'] != 'application/vnd.google-apps.folder':
            virus_total_scan.delay(file)
