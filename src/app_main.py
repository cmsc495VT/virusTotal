# -*- coding: utf-8 -*-
"""Main of the application.

Starts listener for web requests to our application
"""

import flask
import logging
import os
import redis

# Project Imports
from auth import auth_routes
from celery_worker_vt import virus_total_queue, virus_total_content_scan, virus_total_cache
from celery_worker_gd import google_drive_updates
import database
import google_drive
import redisCache
import redisInterface


API_SERVICE_NAME = 'drive'
API_VERSION = 'v2'
app = flask.Flask('app_main')
app.register_blueprint(auth_routes)
# Connect to the redis instance hosted in the docker-compose
if os.environ['STATE'] != 'TESTING':
    REDIS_HOST = os.getenv('REDIS_HOST')
    REDIS_PORT = os.getenv('REDIS_PORT')
    redis_conn = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)
    app.session_interface = redisInterface.RedisSessionInterface(redis=redis_conn)
# Cookie session encryption key, defined in docker .env file
app.secret_key = "{key}".format(key=os.getenv('COOKIE_ENCRYPTION_KEY'))


# Enable error logging when we deploy to the gunicorn handler
if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)


########################
#                      #
#  API Endpoints       #
#                      #
########################
@app.route('/api/1/user/loggedin')
def user_logged_in():
    """Return if user has an active session.

    @author Joel Braun

    API endpoint will be used to determin login status of the user

    Returns: json with {"loggedin": STATUS}
    """
    if 'credentials' not in flask.session:
        return flask.jsonify({'loggedin': 0})
    else:
        return flask.jsonify({'loggedin': 1})


@app.route('/api/1/search', methods=['GET'])
def file_search():
    """Handle queries from the search bar on the FE

    @author Joel Braun

    :returns: JSON results from search

    """
    # Check client Auth
    if 'credentials' not in flask.session:
        flask.abort(415)

    if 'Content-Type' in flask.request.headers and flask.request.headers['Content-Type'] != 'application/json':
        # HTTP 415 Unsupported Media Type
        flask.abort(415)
    user_id = flask.session['credentials']['user_id']
    search_term = flask.request.args.get('searchterm', '')
    search_results = database.search_files(user_id, search_term)
    return flask.jsonify(search_results)


@app.route('/api/1/file')
def file_request():
    """Route for single file data

    @author Joel Braun

    Returns: None
    """
    if 'credentials' not in flask.session:
        flask.abort(415)
    pass


@app.route('/api/1/file/scan', methods=['PUT'])
def file_scan_request():
    """Route for single file scan data

    @author Micah Strube

    Flow
    -------------
    Check client Auth
    Pull file metadata from Mongo
    Queue a content Scan
    Return Initial Status

    Returns: None
    """
    # Check client Auth
    if 'credentials' not in flask.session:
        # Unauthenticated error
        flask.abort(401)

    if flask.request.headers['Content-Type'] != 'application/json':
        # HTTP 415 Unsupported Media Type
        flask.abort(415)

    # Pull file metadata from Mongo
    request_file_data = flask.request.get_json()
    user_id = flask.session['credentials']['user_id']

    app.logger.debug("Data from frontend: {0}".format(request_file_data))

    file_data = database.get_file(user_id, request_file_data['fileId'])

    # Virus Total can only handle 32MB files so cancel if we know it is too large
    if 'size' in file_data and file_data['size'] > 3200000:
        return flask.jsonify({'Status': 'Canceled: File too large'})

    # Queue a content Scan
    virus_total_content_scan.delay(file_data, flask.session['credentials'])

    app.logger.info("Submited file to VirusTotal for content scanning: {fileId}".format(fileId=file_data['fileId']))

    # Return Initial Status
    return flask.jsonify({"status": "Submitted"})


@app.route('/api/1/settings')
def settings_request():
    """Route for getting/setting settings

    @author Joel Braun

    Returns: None
    """
    if 'credentials' not in flask.session:
        # Unauthenticated error
        flask.abort(401)
    pass


@app.route('/api/1/files/scans')
def vt_scan_results_all_files():
    """Retrieve Virus Total scan results from the application for all files.

    @author Joel Braun

    Flow
    ---------
    Check client auth
    Check for cached virus total results
        + Return them
    Check for stored virus total results
        + Cache and return them
    Queue for drive scan
        + queue scan, return []


    Returns: Scan JSON for Front End
    """
    # Check client auth
    # Check if we have an active session
    if 'credentials' not in flask.session:
        # Unauthenticated error
        flask.abort(401)
    else:
        # app.logger.info("Requesting VirusTotal scans for user: {0}".format(flask.session['credentials']['user_id']))
        pass
    cache_id = flask.request.cookies.get(app.session_cookie_name)
    user_id = flask.session['credentials']['user_id']

    # Check for results from the drive
    virus_total_queue.delay(user_id)
    google_drive_updates(flask.session['credentials'])
    # Check for cached virus total results
    #    + Return them
    vt_results = redisCache.virustotal_cache.get_cache(cache_id)
    # update the Cache from the database for later requests
    virus_total_cache.delay(user_id, cache_id)
    if len(vt_results) > 0:
        return flask.jsonify(vt_results)

    vt_results = database.vt_get_scans(user_id)
    if len(vt_results) > 0:
        redisCache.virustotal_cache.save_cache(cache_id, vt_results)
        return flask.jsonify(vt_results)

    # Queue for drive scan
    #    + queue scan, return []
    return flask.jsonify([])


@app.route('/api/1/files')
def google_drive_files():
    """Retrieve google drive files for the Front End.
    Submit files for Virus Total scanning if required.

    @author Joel Braun

    Flow
    ---------
    Check client auth
    Check for cached google drive values
        + Return them
    Check for stored drive results in database
        + Cache and return them
    Retrieve Google Drive results from Google
        + Cache, store in Mongo, return them

    Returns: Scan JSON for Front End
    """
    # Check client auth
    # Check if we have an active session
    if 'credentials' not in flask.session:
        # Unauthenticated error
        flask.abort(401)
    else:
        app.logger.info("Requested files GDrive for user: {0}".format(flask.session['credentials']['user_id']))

    cache_id = flask.request.cookies.get(app.session_cookie_name)
    user_id = flask.session['credentials']['user_id']

    # Check for cached google drive values
    #    + Return them
    # Check for a cache hit for Google Drive results
    cached_drive_data = redisCache.gdrive_cache.get_cache(cache_id)
    if cached_drive_data != []:
        # Send files to VT scanning
        app.logger.info("{1} files in cache for user: {0}".format(user_id,
                                                                  len(cached_drive_data)))
        return flask.jsonify(cached_drive_data)

    # Check for stored drive results in database
    #    + Cache and return them
    stored_files = database.get_files(user_id)
    if len(stored_files) > 0:
        app.logger.info("{1} files in Mongo for user: {0}".format(user_id,
                                                                  len(stored_files)))
        redisCache.gdrive_cache.save_cache(cache_id, stored_files)
        app.logger.info("{1} files added to cache for user: {0}".format(user_id,
                                                                        len(stored_files)))
        return flask.jsonify(stored_files)

    # Retrieve Google Drive results from Google
    #    + Cache, store in Mongo, return them
    # Load credentials from the session.
    file_results = google_drive.gdrive_file_list(flask.session['credentials'])

    # Send file results to Redis cache
    redisCache.gdrive_cache.save_cache(cache_id, file_results)
    app.logger.info("{1} files added to cache for user: {0}".format(user_id,
                                                                    len(file_results)))

    # Send file results to DB
    database.save_files(user_id, file_results)
    app.logger.info("{1} files added to mongo for user: {0}".format(user_id,
                                                                    len(file_results)))

    # Save credentials back to session in case access token was refreshed.

    return flask.jsonify(file_results)


if __name__ == '__main__':
    # Only ran for local testing
    # When running locally, disable OAuthlib's HTTPs verification.
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    # Specify a hostname and port that are set as a valid redirect URI
    # for your API project in the Google API Console.
    app.run('0.0.0.0', 8080, debug=True)
