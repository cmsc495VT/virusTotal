# -*- coding: utf-8 -*-
"""Google Drive utilities for our application

Functions:
gdrive_to_frontend - convert Google Drive JSON from the drive to a dict we use on the frontend

"""
import flask
import google.oauth2.credentials
import googleapiclient.discovery
import logging
import utilities

API_SERVICE_NAME = 'drive'
API_VERSION = 'v2'
logger = logging.getLogger('app_main')


def gdrive_file_list(client_credentials):
    """Get drive listing for the client

    @author Joel Braun

    :client_credentials: Google drive credentials for the user
    :Returns: Dict of files
    """
    credentials = google.oauth2.credentials.Credentials(client_credentials['token'],
                                                        client_credentials['refresh_token'],
                                                        client_credentials['id_token'],
                                                        client_credentials['token_uri'],
                                                        client_credentials['client_id'],
                                                        client_credentials['client_secret'],
                                                        client_credentials['scopes'])
    # Get drive API from good service discovery
    drive = googleapiclient.discovery.build(
        API_SERVICE_NAME, API_VERSION, credentials=credentials)

    # Get all files from google drive
    try:
        files = drive.files().list().execute()
    except google.auth.exceptions.RefreshError:
        # Unauthenticated error
        flask.abort(401)
    client_credentials = utilities.credentials_to_dict(credentials)

    return gdrive_to_frontend(files)


def gdrive_file_content(credentials, fileId, mimeType):
    """
    @author Joel Braun, Micah Strube

    Named Params:
    credentials: Google oauth2 credentials
    fileId: Google Drive fileId for the file
    mimeType: mimeType of the file

    Returns: Binary file content or exported google app file
    """

    # Dictionary mapping Google App mimeType to the mimeType it should be exported as
    google_app_export_dict = {
        'application/vnd.google-apps.document':
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.google-apps.drawing':
            'image/png',
        'application/vnd.google-apps.presentation':
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'application/vnd.google-apps.spreadsheet':
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        }
    # Get drive API from google discovery service
    drive = googleapiclient.discovery.build(
        API_SERVICE_NAME, API_VERSION, credentials=credentials)

    # Get file content from google API
    logger.info("Fetching content for file {fileId} with mimeType {mimeType} from Google Drive".format(fileId=fileId, mimeType=mimeType))
    try:
        # Non Google App files get downloaded as binary
        if mimeType not in google_app_export_dict:
            file_content = drive.files().get_media(fileId=fileId).execute()
        # Google App files must have special cases to export them in a chosen format
        else:
            logger.info("File {fileId} is {mimeType}, exporting as {exportType}".format(fileId=fileId, mimeType=mimeType, exportType=google_app_export_dict[mimeType]))
            file_content = drive.files().export_media(fileId=fileId,
                                                      mimeType=google_app_export_dict[mimeType]).execute()
    except google.auth.exceptions.RefreshError:
        logger.error("Error in refreshing Google drive token")
    return file_content


def gdrive_to_frontend(files):
    """Coherse google's json into our Front End JSON

    @author Joel Braun

    Named Params:
    files: List of files from Google
    """
    if 'items' in files:
        files = files['items']
    cleaned_results = []
    for file in files:
        cleaned_result = dict()
        if 'md5Checksum' in file:
            cleaned_result['md5Checksum'] = file['md5Checksum']
        else:
            cleaned_result['md5Checksum'] = ""
        if 'fileSize' in file:
            cleaned_result['size'] = int(file['fileSize'])
        else:
            cleaned_result['size'] = -1
        if 'title' in file:
            cleaned_result['fileName'] = file['title']
            cleaned_result['fileId'] = file['id']
            cleaned_result['mimeType'] = file['mimeType']
            cleaned_result['modifiedDate'] = file['modifiedDate']
            cleaned_result['createdDate'] = file['createdDate']
        else:
            cleaned_result['filename'] = 'Error - no filename found'
        cleaned_results.append(cleaned_result)
    return cleaned_results
