import google_drive
import unittest

test_file_1 = {'md5Checksum': 'TEST_SUM_1',
               'fileSize': '11',
               'id': 'TEST_ID_1',
               'mimeType': 'applications/pdf',
               'modifiedDate': '2018-04-21T16:51:20.629',
               'createdDate': '2018-04-21T16:51:20.629',
               'title': 'TEST_NAME_1'}

test_file_2 = {'md5Checksum': '',
               'fileSize': '12',
               'id': 'TEST_ID_2',
               'mimeType': 'applications/pdf',
               'modifiedDate': '2018-04-21T16:51:20.629',
               'createdDate': '2018-04-21T16:51:20.629',
               'title': 'TEST_NAME_2'}

test_file_no_size = {'md5Checksum': '',
                     'id': 'TEST_ID_3',
                     'mimeType': 'applications/pdf',
                     'modifiedDate': '2018-04-21T16:51:20.629',
                     'createdDate': '2018-04-21T16:51:20.629',
                     'title': 'TEST_NAME_3'}

test_file_no_title = {'md5Checksum': 'TEST_SUM',
                      'fileSize': '12',
                      'id': 'TEST_ID_4',
                      'mimeType': 'applications/pdf',
                      'modifiedDate': '2018-04-21T16:51:20.629',
                      'createdDate': '2018-04-21T16:51:20.629'}


class BasicTests(unittest.TestCase):

    # ###########################
    # ### setup and teardown ####
    # ###########################

    # executed prior to each test
    def setUp(self):
        pass

    # executed after each test
    def tearDown(self):
        pass

    # ##############
    # ### tests ####
    # ##############
    def test_result_cleaning_vt_to_frontend(self):
        result = []
        result = google_drive.gdrive_to_frontend([test_file_1])
        self.assertEqual(result[0]['size'], 11)
        result = google_drive.gdrive_to_frontend({'items': [test_file_1]})
        self.assertEqual(result[0]['size'], 11)
        result = google_drive.gdrive_to_frontend({'items': [test_file_2]})
        self.assertEqual(result[0]['md5Checksum'], "")
        result = google_drive.gdrive_to_frontend({'items': [test_file_no_size]})
        self.assertEqual(result[0]['size'], -1)
        result = google_drive.gdrive_to_frontend({'items': [test_file_no_title]})
        self.assertEqual(result[0]['filename'], 'Error - no filename found')


if __name__ == "__main__":
    unittest.main()
