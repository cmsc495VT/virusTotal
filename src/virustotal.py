# -*- coding: utf-8 -*-
"""Virus Total Specific implementations for scanning application.

"""
import database
import logging
import requests
from os import getenv

VIRUSTOTAL_KEYS = getenv('VIRUSTOTAL_API_KEYS').split(',')

VIRUSTOTAL_PARAMS = []
for key in VIRUSTOTAL_KEYS:
    vt_params = {'apikey':
                key,
                }
    VIRUSTOTAL_PARAMS.append(vt_params)

logger = logging.getLogger('app_main')

def content_scan(fileId, fileName, file_content):
    """content_scan sends a file's content to Virus Total for scanning

    @author Micah Strube

    Named Params:
    file_content: Contents of file to be scanned

    Returns: raw JSON response from Virus Total
    """

    # Build POST to VirusTotal
    params = VIRUSTOTAL_PARAMS[0]
    files = {'file': (fileName, file_content)}
    response = requests.post('https://www.virustotal.com/vtapi/v2/file/scan',
                             files=files, params=params)
    vt_upload_response = response.json()
    logger.debug(vt_upload_response)
    logger.debug("Content Scan permalink: %s" % vt_upload_response['permalink'])
    md5 = vt_upload_response['md5']
    permalink = vt_upload_response['permalink']
    return md5, permalink


def vt_get_scan_report(fileMd5):
    """Virus Total api call to check for a scan report.

    @author Joel Braun

    Named Params:
    fileMd5: String value of an md5 sum

    Returns: Raw JSON from Virus Total api
    """

    params = VIRUSTOTAL_PARAMS[0]
    headers = {
        "Accept-Encoding": "gzip, deflate",
        "User-Agent": "VirusTotal Google Drive Scan App"
        }
    params['resource'] = fileMd5
    response = requests.get('https://www.virustotal.com/vtapi/v2/file/report',
                            params=params, headers=headers)
    if response.status_code == 200:
        json_response = response.json()
        logger.info("First key successful. Retrieved result for {0} from VT".format(fileMd5))
        return json_response
    else:
        logger.info("VT key 0 failed. Attempting key 1 of {0}".format(len(VIRUSTOTAL_PARAMS)))
        for i in range(1, len(VIRUSTOTAL_PARAMS)):
            params = VIRUSTOTAL_PARAMS[i]
            params['resource'] = fileMd5
            response = requests.get('https://www.virustotal.com/vtapi/v2/file/report',
                                    params=params, headers=headers)
            if response.status_code == 200:
                json_response = response.json()
                logger.info("VT key {0} successful. Retrieved result for {1} from VT".format(i ,fileMd5))
                return json_response
            else:
                logger.info("VT key {0} failed. Attempting key {1} of {2}...".format(i-1, i, len(VIRUSTOTAL_PARAMS)))
        logger.error("ALL VT KEYS FAILED. VIRUS TOTAL ISSUES: {0} {1}".format(response.status_code, response.text))
        return {'response_code': 0}


def vt_check(md5sum):
    """Check if an md5 checksum has a Virus Total report

    @author Joel Braun

    Name Params:
    md5sum: Either a string that is an md5sum or a list of md5sums

    Returns: raw JSON response from Virus Total query
    """
    slice_len = 1
    if isinstance(md5sum, str):
        return vt_get_scan_report(md5sum)
    else:
        scan_result = []
        for start in range(0, len(md5sum), slice_len):
            if start + slice_len > len(md5sum):
                end = len(md5sum)
            else:
                end = start + slice_len
            hash_list = ",".join(md5sum[start:end])
            results = vt_get_scan_report(hash_list)
            scan_result.append(results)
        return scan_result


def vt_to_frontend(result):
    """Coherse a result from Virus Total JSON to our applicartion's JSON.

    @author Joel Braun

    Named Parameters:
    file: File Dicts from Virus Total

    Returns: dict in our application's naming convention
    """
    cleaned_result = dict()
    if 'scan' in result and result['scan']['response_code'] == 1:
        try:
            cleaned_result['scanned'] = 1
            cleaned_result['scanDate'] = result['scan']['scan_date']
            cleaned_result['totalScans'] = result['scan']['total']
            cleaned_result['positiveScanResults'] = result['scan']['positives']
            cleaned_result['permalink'] = result['scan']['permalink']
            cleaned_result['scans'] = result['scan']['scans']
        except KeyError:
            cleaned_result = {'scanned': 0}
    else:
        cleaned_result['scanned'] = 0
    cleaned_result['fileId'] = result['fileId']
    return cleaned_result


def vt_results_to_frontend(files):
    """Coherse the results from Virus Total JSON to our applicartion's JSON.

    @author Joel Braun

    Named Parameters:
    files: list of file Dicts from Virus Total

    Returns: List of dicts in our application's naming convention
    """
    cleaned_results = []
    for result in files:
        if result['mimeType'] != 'application/vnd.google-apps.folder':
            cleaned_results.append(vt_to_frontend(result))
    return cleaned_results


def scan_file(drive_file):
    if 'md5Checksum' in drive_file and drive_file['mimeType'] != 'application/vnd.google-apps.folder':
        if drive_file['md5Checksum'] != "":
            drive_file['scan'] = vt_check(drive_file['md5Checksum'])
            virus_total_result = vt_to_frontend(drive_file)
        else:
            virus_total_result = {"scanned": 0, "fileId": drive_file["fileId"]}
        database.vt_update_file(virus_total_result)
