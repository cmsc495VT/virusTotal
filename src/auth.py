# -*- coding: utf-8 -*-
"""Authenticate the user to the Google Drive API.
Users need to grant their consent for our use of their files. Application
uses this through flask's Blueprint functionality.

To use:
import auth

app.register_blueprint(auth_routes)

"""
import flask
import logging
import google.oauth2.credentials
import google_auth_oauthlib.flow
import os
import requests
import utilities


# The CLIENT_SECRETS variable specifies the  the OAuth 2.0 information for this
# application, including its client_id and client_secret.
CLIENT_SECRETS_FILE = os.getenv('CLIENT_SECRETS_FILE')
# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly',
          'https://www.googleapis.com/auth/drive.readonly',
          'profile']

auth_routes = flask.Blueprint('auth_route', '__name__')
logger = logging.getLogger('app_main')
hostname = os.environ['HOSTNAME']
authorize_url = hostname + "/authorize"


@auth_routes.errorhandler(401)
def not_authenticated(error):
    return flask.Response('Failed to authenticate with google', 401)


@auth_routes.route('/authorize')
def authorize():
    """Authorize page OAUTH2 workflow

    @author Joel Braun

    Returns: A redirect to the oauth2 page
    """
    # Create flow instance to manage the OAuth 2.0 Authorization
    # Grant Flow steps.
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES)
    flow.redirect_uri = "{0}/oauth2callback".format(hostname)

    authorization_url, state = flow.authorization_url(
        # Enable offline access so that you can refresh an access token without
        # re-prompting the user for permission. Recommended for web server apps
        access_type='offline',
        # Enable incremental authorization. Recommended as a best practice.
        include_granted_scopes='true')

    # Store the state so the callback can verify the auth server response.
    flask.session['state'] = state

    return flask.redirect(authorization_url)


@auth_routes.route('/oauth2callback')
def oauth2callback():
    """oauth2callback page OAUTH2 workflow

    @author Joel Braun

    Returns: A redirect to the root main page
    """
    # Specify the state when creating the flow in the callback so that it can
    # verified in the authorization server response.
    logger.info(flask.session)
    logger.info("{0}".format(flask.session.__dict__))
    state = flask.session['state']

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES, state=state)

    flow.redirect_uri = "{0}/oauth2callback".format(hostname)
    # flow.redirect_uri = flask.url_for('auth_route.oauth2callback', _external=True)

    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = flask.request.url
    flow.fetch_token(authorization_response=authorization_response)

    # Store credentials in the session.
    # ACTION ITEM: In a production app, you likely want to save these
    #              credentials in a persistent database instead.
    credentials = flow.credentials
    flask.session['credentials'] = utilities.credentials_to_dict(credentials)

    return flask.redirect('{0}/'.format(hostname))


def revoke():
    """Revoke credentials at Google from the OAUTH2 workflow

    @author Joel Braun

    Returns: status message from clearing
    """
    if 'credentials' not in flask.session:
        return ('You need to <a href="/authorize">authorize</a> before ' +
                'testing the code to revoke credentials.')

    credentials = google.oauth2.credentials.Credentials(flask.session['credentials']['token'],
                                                        flask.session['credentials']['refresh_token'],
                                                        flask.session['credentials']['id_token'],
                                                        flask.session['credentials']['token_uri'],
                                                        flask.session['credentials']['client_id'],
                                                        flask.session['credentials']['client_secret'],
                                                        flask.session['credentials']['scopes'])

    # Get drive API from good service discovery
    revoke = requests.post('https://accounts.google.com/o/oauth2/revoke',
                           params={'token': credentials.token},
                           headers={'content-type': 'application/x-www-form-urlencoded'})

    status_code = getattr(revoke, 'status_code')
    if status_code == 200:
        return('Credentials successfully revoked.')
    else:
        return('An error occurred.')


def clear_credentials():
    """Clear credentials from local store for the OAUTH2 workflow

    @author Joel Braun

    Returns: status message from clearing
    """
    if 'credentials' in flask.session:
        del flask.session['credentials']
    return ('Credentials have been cleared.<br><br>')
