import utilities
import unittest


class TestCred(object):
    id_token = 'TEST_ID_TOKEN'
    token = 'TEST_TOKEN'
    refresh_token = 'TEST_REFRESH'
    token_uri = 'TEST_URI'
    client_id = 'TEST_ID'
    client_secret = 'TEST_SECRET'
    scopes = 'TEST_SCOPE'

    def __init__(self):
        pass


class BasicTests(unittest.TestCase):

    # ###########################
    # ### setup and teardown ####
    # ###########################

    # executed prior to each test
    def setUp(self):
        pass

    # executed after each test
    def tearDown(self):
        pass

    # ##############
    # ### tests ####
    # ##############

    def test_cred_parse_to_dict(self):
        result = utilities.credentials_to_dict(TestCred())
        self.assertEqual(result['token'], 'TEST_TOKEN')


if __name__ == "__main__":
    unittest.main()
