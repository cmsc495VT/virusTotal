# -*- coding: utf-8 -*-
"""General purpose functions

"""
from google.oauth2 import id_token
from google.auth.transport import requests as google_requests
import logging

logger = logging.getLogger('app_main')


def get_user_id(credentials):
    """Verify token and get obfuscated user id from Google

    @author Micah Strube

    credentials: google oauth2 Credentials

    Returns:
    """
    try:
        idinfo = id_token.verify_oauth2_token(credentials.id_token,
                                              google_requests.Request(),
                                              credentials.client_id)
        """
        Example id_info = {
            "iss": "https://accounts.google.com",
            "sub": "110169484474386276334",
            "azp": "1008719970978-hb24n2dstb40o45d4feuo2ukqmcc6381.apps.googleusercontent.com",
            "aud": "1008719970978-hb24n2dstb40o45d4feuo2ukqmcc6381.apps.googleusercontent.com",
            "iat": "1433978353",
            "exp": "1433981953"
        }
        See https://developers.google.com/identity/sign-in/android/backend-auth
        """
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            logger.info("WRONG ISSUER: {0}".format(idinfo['iss']))
            raise ValueError('Wrong issuer.')
        return idinfo['sub']
    except ValueError:
        logger.info("Invalid token: {0}".format(credentials.id_token))


def credentials_to_dict(credentials):
    """Normalize credentials from Google for storage

    @author Joel Braun

    Named Params:
    credentials: tokens from Google

    Return: a dict for Redis to store
    """

    user_id = get_user_id(credentials)

    return {'token': credentials.token,
            'user_id': user_id,
            'refresh_token': credentials.refresh_token,
            'id_token': credentials.id_token,
            'token_uri': credentials.token_uri,
            'client_id': credentials.client_id,
            'client_secret': credentials.client_secret,
            'scopes': credentials.scopes}
