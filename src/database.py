# -*- coding: utf-8 -*-
"""Main of the application.

Starts listener for web requests to our application
"""

import copy
import logging
from pymongo import MongoClient
import re
from os import getenv

# Mongo connection
client = MongoClient(getenv('MONGO_URL'))
db = client.vtgdrive

logger = logging.getLogger('app_main')


def search_files(user_id, query_term):
    """Search the stored results from the database

    Provide some smarter search, wildcard search the filename
    md5:HASH
    filename:Term

    @author Joel Braun

    :user_id: User Identifier
    :query_term: Term from the FE to search for files against
    :returns: Dict of file results for the FE to display
    """
    files_collection = db.files
    search_dorks = ["md5", "filename", "result"]
    parsed_terms = re.search(r"^({0}):(.+)".format("|".join(search_dorks)), query_term)
    if parsed_terms is not None:
        query_term = parsed_terms.group(2)
        if parsed_terms.group(1) == "filename":
            regex_term = re.compile(".*{0}.*".format(query_term), re.IGNORECASE)
            file_list = files_collection.find({'user_id': user_id, 'fileName': regex_term})
        elif parsed_terms.group(1) == "md5":
            regex_term = re.compile("^{0}.*".format(query_term), re.IGNORECASE)
            file_list = files_collection.find({'user_id': user_id, 'md5Checksum': regex_term})
        elif parsed_terms.group(1) == "result":
            if parsed_terms.group(2) == "good":
                file_list = files_collection.find({'user_id': user_id, 'scan.positiveScanResults': 0})
            elif parsed_terms.group(2) == "bad":
                file_list = files_collection.find({'user_id': user_id, 'scan.positiveScanResults': {'$gt': 0}})
            elif parsed_terms.group(2) == "none":
                file_list = files_collection.find({'user_id': user_id, 'scan.scanned': 0})
    else:
        logger.info(query_term)
        regex_term = re.compile(".*{0}.*".format(query_term), re.IGNORECASE)
        file_list = files_collection.find({'user_id': user_id,
                                           'fileName': {'$regex': regex_term}})

    files = []
    for file_record in file_list:
        file_record.pop('_id')
        files.append(file_record)
    return files


def save_files(user_id, file_list):
    """Save user's files to the database.

    @author Joel Braun

    :user_id: User Identifier
    :file_list: List of files from google drive
    :returns: N/A
    """
    files_collection = db.files
    for file in file_list:
        file['user_id'] = user_id
        files_collection.update({'user_id': user_id, 'fileId': file['fileId']}, {'$set': copy.copy(file)}, upsert=True)


def get_file(user_id, fileId):
    """Retrieve a file stored in the database

    @author Joel Braun

    :user_id: Client ID for the user
    :fileId: fileId from google for the file
    :returns: List of files stored in the database
    """
    files_collection = db.files
    file_dict = files_collection.find_one({'user_id': user_id, 'fileId': fileId})
    file_dict.pop('_id')
    return file_dict


def get_files(user_id):
    """Retrieve a users files stored in the database

    @author Joel Braun

    :user_id: Client ID for the user
    :returns: List of files stored in the database
    """
    files_collection = db.files
    files = []
    file_list = files_collection.find({'user_id': user_id})
    for file_record in file_list:
        file_record.pop('_id')
        files.append(file_record)
    return files


def vt_get_scans(user_id):
    """Retrieve stored virus total scan results for user

    :user_id: TODO
    :returns: TODO

    """
    user_files = get_files(user_id)
    scans = []
    for file in user_files:
        if 'scan' in file:
            scans.append(file['scan'])
    return scans


def vt_update_file(report):
    """Update documents with Virus total results

    @author Joel Braun

    :report_list: Array of results from a mass vt scan
    :returns: None

    """
    files_collection = db.files
    files_collection.update_one({'fileId': report['fileId']}, {"$set": {'scan': report}}, upsert=False)


def vt_update_files(report_list):
    """Update document with Virus total results

    @author Joel Braun

    :report_list: Array of results from a mass vt scan
    :returns: None

    """
    for report in report_list:
        vt_update_file(report)


def update_file_md5(fileId, md5):
    """Update md5sum for a single file

    @author Micah Strube

    :fileId: Google fileId of the file to update
    :md5: md5 checksum to add to the file
    :returns: None

    """
    files_collection = db.files
    files_collection.update_one({'fileId': fileId}, {"$set": {'md5Checksum': md5}}, upsert=False)


def update_file_scanned_status(fileId, scanned_status):
    """Update the scanned status for a single file.

    @author Micah Strbe

    :fileId: Google fileId of the file to update
    :scanned_status: The value to set as the files scanned status.
                     0  = unscanned
                     1  = scanned
                     -1 = scan in progress
    :returns: None
    """
    files_collection = db.files
    files_collection.update_one({'fileId': fileId},
                                {"$set":
                                    {'scan.scanned': scanned_status}
                                 },
                                upsert=False)
