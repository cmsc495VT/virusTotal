Json Interface
----------------------

Google gives us data about our files in their JSON format. We only need
a few fields from their data for our use. 
    - id
        - Unique id for file
    - modifiedDate
        - Date modified
    - originalFilename
        - Name of file
    - md5Checksum
        - MD5 of file's contents
    - mimeType
        - Type of file that google thinks it is
    - alternateLink 
        - Link to the file
        - possibly use this to give to VT instead of
        downloading to the server then uploading to VT.

Virus Total gives us data in their JSON for requests.
    - response_code
        - 0: report not found
        - 1: Report found 
    - scan_date
        - Date scan was done, this isn't our date from md5 just the last time
            anyone submitted this content
    - scans
        - Scan results from each vendor
    - total 
        - Number of virus scans done
    - positives
        - Number of scans detecting as malicous
    - permalink
        - Link to VT report page


